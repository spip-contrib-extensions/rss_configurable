<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute les metas sauvegardables de rssconfig pour le plugin IEConfig
 *
 * @pipeline ieconfig_metas
 *
 * @param array $table
 *     Déclaration des sauvegardes
 * @return array
 *     Déclaration des sauvegardes completees
 **/
function rssconfig_ieconfig_metas($table) {
	$table['rssconfig']['titre'] = _T('rssconfig:rssconfig');
	$table['rssconfig']['icone'] = 'prive/themes/spip/images/rssconfig-16.png';
	$table['rssconfig']['metas_serialize'] = 'rssconfig';

	return $table;
}
