<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'rssconfig_description' => 'Configurer avec précision le fonctionnement de votre flux RSS à l’aide d’un formulaire dans l’espace privé.',
	'rssconfig_slogan' => 'Personnaliser le flux RSS des articles du site',
);
